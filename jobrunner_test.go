package jobrunner

import (
	"fmt"
	"testing"
	"time"
)

/**
 *
 * @Author
 * @date
 * @Description
 *
 **/

type ScriptJob struct {
	Name string
}

func (receiver ScriptJob) Run() {
	fmt.Println("Name----", receiver.Name)
}

func TestNewTimerTask(t *testing.T) {
	t.Log("测试开始")
	Start()
	if err := Schedule("0/2 * * * * ?", ScriptJob{Name: "test1"}); err != nil {
		t.Error(err)
	}
	if err := Schedule("@every 2s", ScriptJob{Name: "test2"}); err != nil {
		t.Error(err)
	}

	time.Sleep(10 * time.Second)
}
